const yargs = require("yargs");
const { simpanContact } = require("./contacts");

yargs.command({
    command: 'add',
    describe: 'Menambahkan contact baru',
    builder: {
        nama: {
            describe: 'Masukan nama : ',
            demandOption: true,
            type: 'string',
        },
        noHp: {
            describe: 'Masukan nomor Handphone : ',
            demandOption: true,
            type:'string'
        },
        email: {
            describe: 'Masukan email: ',
            demandOption: false,
            type:'string'
        },
    },
    handler(argv) { 
        simpanContact(argv.nama,argv.noHp,argv.email)
    }
})

yargs.parse()
// const { simpanContact, tulisPertanyaan } = require("./contacts")

// const main = async () => { 
//     const nama = await tulisPertanyaan('Masukan Nama Anda: ')
//     const noHp = await tulisPertanyaan('Masukan Nomor Handphone Anda: ')
//     const email = await tulisPertanyaan('Masukan Email Anda: ')

//     simpanContact(nama, noHp, email)
// }

// main()
