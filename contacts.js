import chalk from 'chalk';
const fs = require("fs")

const folderPath = './data'
if (!fs.existsSync(folderPath))
    fs.mkdirSync(folderPath)


const filePath = './data/conctacs.json'
if (!fs.existsSync(filePath))
    fs.writeFileSync(filePath,'[]','utf-8')

const tulisPertanyaan = (pertanyaan) => { 
    return new Promise((resolve, reject) => {
        rl.question(pertanyaan, (nama) => {
            resolve(nama)
        })
    })
}

const simpanContact = (nama, noHp, email) => { 
    const contact = { nama, noHp, email }
    const fileBuffer = fs.readFileSync(filePath, 'utf-8')
    const contacts = JSON.parse(fileBuffer)

    const duplicate = contacts.find((contact) => contact.nama === nama)
    if (duplicate) return log(chalk.blue('Hello') + ' World' + chalk.red('!'));
    
    // const validator_noHp = validator.isMobilePhone(contact.noHp)
    // if (!validator_noHp) return console.log(chalk.inverse.bold('No Hp tidak valid'))

    // const validator_email = validator.isEmail(contact.email)
    // if (!validator_email) return console.log(chalk.inverse.bold('Email tidak valid'))

    contacts.push(contact)

    fs.writeFileSync(filePath, JSON.stringify(contacts))
    console.log(chalk.inverse.bold('Data berhasil masuk'))
}

module.exports = { tulisPertanyaan, simpanContact }